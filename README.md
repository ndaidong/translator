# translator

Translate service using Google Cloud Translate API.

## Setup

```
git clone https://gitlab.com/ndaidong/translator.git
cd translator
python3 -m venv venv
source venv/bin/activate
(venv) pip install -r requirements.txt
# you must specify required env variables before running server
(venv) python server.py
```

### Environment variables

 - GOOGLE_APPLICATION_CREDENTIALS: required, path to credential file ([read](https://cloud.google.com/docs/authentication/production))
 - KEY: private key to access API, if not specify, it would generate a random key
 - PORT: port of service, default is 7535
 - ENV: environment, default is 'development'


## API

- End-point: `http://0.0.0.0:7535/translate`
- GET/POST is ok
- Parameters:
    - `text`: text content to translate
    - `to`: target language in ISO-639-1 Code format ([view list](https://cloud.google.com/translate/docs/languages))
    - `key`: API key

#### Examples:

```
GET http://0.0.0.0:7535/translate?text=hello&to=vi&key=API_KEY

POST http://0.0.0.0:7535/translate

 Request header:
  {
    "content-type": "application/json",
    "key": "79797399",
  }

 Request body:
  {
    "text": "Hello, how are you?",
    "to": "zh"
  }

```



## License

The MIT License (MIT)
