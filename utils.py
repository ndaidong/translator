#!/usr/bin/env python3

import xxhash

from lruttl import LRUCache
from google.cloud import translate

translate_client = translate.Client()

cache = LRUCache(100)


def hash(string):
    return xxhash.xxh64(string).hexdigest()


def translate(text, to):
    key = hash(text + '-' + to)
    stored = cache[key]
    if stored:
        return stored
    translation = ''
    try:
        result = translate_client.translate(
            text,
            target_language=to)
        translation = result['translatedText']
        cache.set(key, translation)
    except Exception:
        pass
    return translation
