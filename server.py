#!/usr/bin/env python3

from os import getenv

from flask import Flask
from flask_restful import Resource, Api

from resources import Translator

ENV = getenv('ENV', 'development')
PORT = getenv('PORT', 7535)


DEBUG = True
if ENV == 'production':
    DEBUG = False


app = Flask(__name__)
api = Api(app, catch_all_404s=True)

api.add_resource(Translator, '/translate', '/api/translate')

if __name__ == '__main__':
    app.run(debug=DEBUG, host='0.0.0.0', port=PORT)
