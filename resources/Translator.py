#!/usr/bin/env python3

from os import getenv
from random import randrange

from flask_restful import Resource, reqparse

from utils import translate

parser = reqparse.RequestParser()
parser.add_argument('key', location=['headers', 'values'])
parser.add_argument('text', type=str, location=['json', 'values'])
parser.add_argument('to', type=str, location=['json', 'values'])


KEY = getenv('KEY', randrange(1000000, 9999999))
print('API key is {}'.format(KEY))


def resolve(self):
    args = parser.parse_args()
    key = str(args['key'])
    text = str(args['text'])
    to = str(args['to'])
    if to == 'None':
        to = 'vi'
    if key != KEY:
        return {
            'error': 1,
            'message': 'Invalid key',
        }

    error = 0
    message = 'Translated successfully'
    translation = text
    if len(text) > 2:
        result = translate(text, to)
        if result == '':
            error = 1
            message = 'Could not translate'
        else:
            translation = result

    return {
        'error': error,
        'message': message,
        'text': text,
        'to': to,
        'translation': translation,
    }


class Translator(Resource):
    def post(self):
        return resolve(self)

    def get(self):
        return resolve(self)
